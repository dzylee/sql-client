﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace part3_code
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            //Populate the drop-down list with all the available colours in the database:
            this.updateDropDownList();
        }

        private void comboBox1_Click(object sender, EventArgs e)
        //This event handler is executed each time the drop-down list is clicked.
        {
            //Populate the drop-down list with all the available colours in the database:
            this.updateDropDownList();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        //This event handler is executed each time the selection in the drop-down list changes.
        //It will query the SQL Server to get a relation that contains the highest price that each customer paid for any product of a given colour.
        //Then it will display the resulting relation to screen in a grid view and in a text box, and also write the information to a text file, in comma-separated format.
        {
            //Create a connection string to cypress.csil.sfu.ca:
            string awConnStr = @"Data Source=cypress.csil.sfu.ca;Database=zylee354;Integrated Security=false;User ID=username;Password=password;Connection Timeout=30";
            //Create a connection object using the connection string:
            SqlConnection awConn = new SqlConnection(awConnStr);
            //Open the connection to the database:
            awConn.Open();

            //Create the query string. This is the SQL query to be reproduced:
            //      SELECT C.CustomerID AS Customer_ID, C.FirstName AS FirstName, C.LastName AS LastName, C.EmailAddress AS EmailAddress, MAX(D.UnitPrice) AS HighestPrice
            //      FROM SalesLt.Customer C, SalesLT.SalesOrderHeader H, SalesLT.SalesOrderDetail D, SalesLT.Product P
            //      WHERE C.CustomerID=H.CustomerID AND H.SalesOrderID=D.SalesOrderID AND D.ProductID=P.ProductID AND P.Color='<COLOUR>'
            //      GROUP BY C.CustomerID, C.FirstName, C.LastName, C.EmailAddress
            //      ORDER BY C.CustomerID ASC
            //
            string queryString =
            @"SELECT C.CustomerID AS Customer_ID, C.FirstName AS FirstName, C.LastName AS LastName, C.EmailAddress AS EmailAddress, MAX(D.UnitPrice) AS HighestPrice " +
            "FROM SalesLt.Customer C, SalesLT.SalesOrderHeader H, SalesLT.SalesOrderDetail D, SalesLT.Product P " +
            "WHERE C.CustomerID=H.CustomerID AND H.SalesOrderID=D.SalesOrderID AND D.ProductID=P.ProductID AND P.Color='" + this.comboBox1.Text + "' " +
            "GROUP BY C.CustomerID, C.FirstName, C.LastName, C.EmailAddress " +
            "ORDER BY C.CustomerID ASC";

            //Create an SqlDataAdapter object. The query string and the connection object are supplied as arguments to the constructor:
            SqlDataAdapter awAdapter = new SqlDataAdapter(queryString, awConn);
            //Create a DataSet object to hold the results of the query:
            DataSet colourSpendingDataSet = new DataSet();
            //Use the SqlDataAdapter object to fill the DataSet object with the relation produced by the query, and name the relation "ColourSpending":
            awAdapter.Fill(colourSpendingDataSet, "ColourSpending");

            //Display the result to screen in a DataGridView:
            this.dataGridView1.DataSource = colourSpendingDataSet.Tables["ColourSpending"];

            //A plain comma-separated version of the information in the DataSet object will also be displayed to screen in a text box and also written to a file (ColourSpendingOutputFile.txt). The output file is created in the same directory as the program's executable.
            //First create a FileStream object:
            FileStream outputFile = new FileStream(@".\ColourSpendingOutputFile.txt", FileMode.Create);
            //Then create a StreamWriter object using the FileStream object:
            StreamWriter sw = new StreamWriter(outputFile);

            //Display the relation in a Textbox, as plain comma-separated text:
            this.textBox1.Text = "";
            this.textBox1.Text = "The following information has also been written to file\r\n";
            this.textBox1.Text += "(ColourSpendingOutputFile.txt). The file is located in the same\r\n";
            this.textBox1.Text += "directory as this program's executable file.\r\n\r\n";

            foreach (DataRow theRow in colourSpendingDataSet.Tables["ColourSpending"].Rows)
            {
                //Display the information to screen in the text box:
                this.textBox1.Text += theRow[0] + ", " + theRow[1] + ", " + theRow[2] + ", " + theRow[3] + ", " + theRow[4] + "\r\n";

                //Write the information to a text file:
                sw.WriteLine(theRow[0] + ", " + theRow[1] + ", " + theRow[2] + ", " + theRow[3] + ", " + theRow[4]);
            }

            //Flush any contents that might remain in the stream buffer to file:
            sw.Flush();

            //Close the StreamWriter object:
            sw.Close();
            //Close the connection to the database:
            awConn.Close();
        }

        private void updateDropDownList()
        //This method is used to repopulate the drop-down list with an updated list of available colours from the database.
        {
            //Create a connection string to cypress.csil.sfu.ca:
            string awConnStr = @"Data Source=cypress.csil.sfu.ca;Database=zylee354;Integrated Security=false;User ID=username;Password=password;Connection Timeout=30";
            //Create a connection object using the connection string:
            SqlConnection awConn = new SqlConnection(awConnStr);
            //Open the connection to the database:
            awConn.Open();

            //Create a command object to store an SQL query:
            SqlCommand getColoursCommand = awConn.CreateCommand();
            //Set the SQL query to the command object:
            getColoursCommand.CommandText = @"SELECT DISTINCT P.Color FROM SalesLT.Product P WHERE P.Color<>'NULL'";

            //Execute the command stored in the command object, and store the result in a reader object:
            SqlDataReader awReader = getColoursCommand.ExecuteReader();

            //Clear the list of entries in the drop-down list:
            this.comboBox1.Items.Clear();

            //Repopulate the drop-down list with the values in the reader object:
            while (awReader.Read())
            {
                //The index number in the array refers to the column in the query result. Counting starts at zero:
                this.comboBox1.Items.Add(awReader[0]);
            }

            //Close the reader:
            awReader.Close();
            //Close the connection to the database:
            awConn.Close();
        }
    }
}